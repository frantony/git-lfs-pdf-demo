GitLab git-lfs demo
===================

1. Create blank project (https://gitlab.com/projects/new#blank_project),
fill the "Project name" for with "git-lfs-pdf-demo",
uncheck "Initialize repository with a README", next press "Create project".

2. Clone repo, make initial commit and push it to the gitlab server:

```
git clone gitlab:frantony/git-lfs-pdf-demo
Cloning into 'git-lfs-pdf-demo'...
warning: You appear to have cloned an empty repository.

cd git-lfs-pdf-demo
git lfs install
git lfs track "*.pdf"
mkdir pdf
cd pdf
wget https://download.rockbox.org/daily/manual/rockbox-sansaclipplus.pdf
git add *.pdf
git add ../.gitattributes
git commit -s -m "Initial commit"
git push
```

3. Go to https://gitlab.com/frantony/git-lfs-pdf-demo/-/tree/main/pdf, use "This directory" -> "Upload file" dialog.

4. Select file to upload, fill the "Commit message" form, next press the "Upload file" button

5. Enjoy new file in the pdf directory stored with git LFS!

